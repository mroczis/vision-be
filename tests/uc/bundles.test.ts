import {expect} from 'chai';
import {CreateOrUpdateBundleUC} from "../../src/uc/CreateOrUpdateBundleUC";
import {GetAllBundlesUC} from "../../src/uc/GetAllBundlesUC";
import {DeleteBundleUC} from "../../src/uc/DeleteBundleUC";
import {GetBundleUC} from "../../src/uc/GetBundleUC";

describe('Bundle CRUD operations', () => {
    it('List all, create one, list again and delete created', async () => {
        // List all
        const allItemsBefore = await new GetAllBundlesUC().execute(
            new GetAllBundlesUC.Request(20, 0, '')
        ).bundles

        // Create a new one
        const newBundle = await new CreateOrUpdateBundleUC().execute(
            new CreateOrUpdateBundleUC.Request(
                null, 'uc-test-key', 'UC Test Name', [
                    {name: 'item', url: 'https://website/image.png', isStart: true},
                    {name: 'item2', url: 'https://website/image2.png', isStart: false}
                ]
            )
        ).bundle

        // List all
        const allItemsAfter = await new GetAllBundlesUC().execute(
            new GetAllBundlesUC.Request(20, 0, '')
        ).bundles

        // Total count must be 1 higher
        expect(allItemsBefore.count.toString()).to.be.eq((allItemsAfter.count - 1).toString())

        // Bundle must exist and can be found by its key
        const getBundleAfterCreated = await new GetBundleUC().execute(
            new GetBundleUC.Request('uc-test-key')
        ).bundle

        expect(getBundleAfterCreated).to.be.not.null

        // Delete it
        await new DeleteBundleUC().execute(new DeleteBundleUC.Request(newBundle!!.id))
        await new Promise(resolve => setTimeout(resolve, 100));

        const getBundleAfterDeleted = await new GetBundleUC().execute(
            new GetBundleUC.Request('uc-test-key')
        ).bundle

        const allItemsFinal = await new GetAllBundlesUC().execute(
            new GetAllBundlesUC.Request(20, 0, '')
        ).bundles

        // In the end we should not be able to get it once again
        expect(getBundleAfterDeleted).to.be.null

        // And count of items should be the same as in start
        expect(allItemsBefore.count).to.be.eq(allItemsFinal.count)

    })
})
