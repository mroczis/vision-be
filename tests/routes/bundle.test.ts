import {expect} from 'chai';
import app from '../../src/app';
import {agent as request} from 'supertest';
import {BASE64_VALID_AUTH_HEADER} from "./constants";

describe('Bundle tests', () => {
    it('GET /bundles returns nothing without authorization', async () => {
        const response = await request(app).get('/bundle');
        expect(response.status).to.equal(401)
    })

    it('GET /bundles returns bundles with authorization', async () => {
        const response = await request(app)
            .get('/bundle')
            .set('Authorization', BASE64_VALID_AUTH_HEADER);

        expect(response.status).to.equal(200)
        expect(response.body).to.be.an.instanceof(Object);
        expect(response.body).to.have.property('count');
        expect(response.body).to.have.property('bundles');
    })

    it('Create bundle, check if exists afterwards, and delete it', async () => {
        const responsePost = await request(app)
            .post('/bundle')
            .set('Authorization', BASE64_VALID_AUTH_HEADER)
            .send({
                name: 'test',
                key: 'test-key',
                items: [
                    {
                        name: 'Test item',
                        url: 'https://vision.ui/test/img.png',
                        isStart: true
                    }
                ]
            })

        expect(responsePost.status).to.equal(200)
        expect(responsePost.body).to.be.an.instanceof(Object);
        expect(responsePost.body).to.have.property('id');
        const id = responsePost.body.id

        const responseGet = await request(app)
            .get('/bundle/test-key')
            .set('Authorization', BASE64_VALID_AUTH_HEADER)

        expect(responseGet.status).to.equal(200)
        expect(responseGet.body).to.be.an.instanceof(Object);
        expect(responseGet.body).to.have.property('id');
        expect(id).to.be.eq(responseGet.body.id)

        const responseDelete = await request(app)
            .delete('/bundle/' + id)
            .set('Authorization', BASE64_VALID_AUTH_HEADER)

        expect(responseDelete.status).to.equal(204)
    })
})
