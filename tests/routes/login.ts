import {expect} from 'chai';
import app from '../../src/app';
import {agent as request} from 'supertest';
import {BASE64_INVALID_AUTH_HEADER, BASE64_VALID_AUTH_HEADER} from "./constants";

describe('Authorization tests', () => {
    it('GET /login with no auth header', async () => {
        const response = await request(app)
            .get('/login');

        expect(response.status).to.equal(401)
    })

    it('GET /login with OK auth header', async () => {
        const response = await request(app)
            .get('/login')
            .set('Authorization', BASE64_VALID_AUTH_HEADER);

        expect(response.status).to.equal(204)
    })

    it('GET /login with wrong auth header', async () => {
        const response = await request(app)
            .get('/login')
            .set('Authorization', BASE64_INVALID_AUTH_HEADER);

        expect(response.status).to.equal(401)
    })
})
