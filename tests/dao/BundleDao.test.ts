import {expect} from "chai";
import {BundleDao} from "../../src/dao/BundleDao";

describe('BundleDao tests', () => {
    it('Create and delete item', async () => {
        const dao = new BundleDao()
        const bundle = await dao.insert("name", "key", null)

        expect(bundle.id).to.be.greaterThan(0)
        expect(bundle.name).to.equal("name")
        expect(bundle.key).to.equal("key")
        expect(isNaN(bundle.startItemId)).to.equal(true)

        const deleted = await dao.delete(bundle.id)
        expect(deleted).to.be.equal(true)
    })

    it('Update item', async () => {
        const dao = new BundleDao()
        const bundle = await dao.insert("name", "key", null)
        const updated = await dao.update("name-2", "key-2", bundle.id)

        expect(updated.id).to.equal(bundle.id)
        expect(updated.name).to.equal("name-2")
        expect(updated.key).to.equal("key-2")

        const deleted = await dao.delete(bundle.id)
        expect(deleted).to.be.equal(true)

    })
})
