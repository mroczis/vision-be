/**
 * Parses number from string. If process fails then default value is returned.
 */
export function getNumber(value: any, defaultValue: number) {
    const num = parseInt(value, 10);
    return isNaN(num) ? defaultValue : num;
}
