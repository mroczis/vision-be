import {pool} from "../app";
import {QueryResult} from "pg";
import {BundleItem} from "../model/BundleItem";

/**
 * Data access object for bundle items manipulation
 */
export class BundleItemDao {

    getByBundle(bundle: number): Promise<BundleItem[]> {
        return new Promise<BundleItem[]>((resolve => {
            pool.query(`SELECT id, bundle, name, url FROM item WHERE bundle = $1`, [bundle], (error: Error, res: QueryResult) => {
                resolve(this.convert(res))
            })
        }))
    }

    delete(id: number): Promise<Boolean> {
        return new Promise<Boolean>((resolve => {
            pool.query(`DELETE FROM item WHERE id = $1`, [id], (error: Error, res: QueryResult) => {
                resolve(error === null || error === undefined)
            })
        }))
    }

    deleteByBundle(id: number) {
        return new Promise<Boolean>((resolve => {
            pool.query(`UPDATE bundle SET start_item_id = NULL WHERE id = $1`, [id],(error: Error, res: QueryResult) => {
                pool.query(`DELETE FROM item WHERE bundle = $1`, [id], (error: Error, res: QueryResult) => {
                    resolve(error === null || error === undefined)
                })
            })

        }))
    }

    insert(bundle: number, name: string, url: string): Promise<BundleItem> {
        return new Promise<BundleItem>((resolve => {
            pool.query(`INSERT INTO item (name, url, bundle)
                        VALUES ($1, $2, $3)
                        RETURNING id, bundle, name, url`, [name, url, bundle], (error: Error, res: QueryResult) => {
                resolve(this.convert(res)[0])
            })
        }))
    }

    private convert(res: QueryResult): BundleItem[] {
        return res.rows.map((row) => {
            return new BundleItem(row["id"], row["name"], row["url"])
        })
    }
}


