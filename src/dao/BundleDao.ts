import {pool} from "../app";
import {QueryResult} from "pg";
import {Bundle} from "../model/Bundle";

/**
 * Data access object for bundle manipulation
 */
export class BundleDao {

    getAll(limit: number = 100, offset: number = 0, query: string | undefined = undefined): Promise<Bundle[]> {
        return new Promise<Bundle[]>((resolve => {
            pool.query(`SELECT b.id, b.name, b.key, b.start_item_id
                            FROM bundle AS b
                            WHERE (b.name ILIKE $1 OR b.key ILIKE $2)
                            ORDER BY b.id DESC
                            LIMIT $3
                            OFFSET
                            $4 `,
                [`%${query}%`, `%${query}%`, limit, offset], (error: Error, res: QueryResult) => {
                    resolve(this.convert(res))
                })
        }))
    }

    getAllCount(query: string | undefined = undefined): Promise<number> {
        return new Promise<number>((resolve => {
            pool.query(`SELECT COUNT(*) FROM bundle AS b WHERE (b.name ILIKE $1 OR b.key ILIKE $2)`,
                [`%${query}%`, `%${query}%`], (error: Error, res: QueryResult) => {
                    resolve(res.rows.pop()["count"] as number)
                })
        }))
    }

    getByKey(key: string): Promise<Bundle | undefined> {
        return new Promise<Bundle | undefined>((resolve => {
            pool.query(`SELECT b.id, b.name, b.key, b.start_item_id
                        FROM bundle AS b
                        WHERE b.key = $1`, [key], (error: Error, res: QueryResult) => {
                resolve(this.convert(res).pop())
            })
        }))
    }

    delete(id: number): Promise<boolean> {
        return new Promise<boolean>((resolve => {
            pool.query(`DELETE
                        FROM bundle
                        WHERE id = $1`, [id], (error: Error, res: QueryResult) => {
                resolve(error === null || error === undefined)
            })
        }))
    }

    insert(name: string, key: string, startItemId: number | null = null): Promise<Bundle> {
        return new Promise<Bundle>((resolve => {
            pool.query(`INSERT INTO bundle (name, key, start_item_id)
                        VALUES ($1, $2, $3)
                        RETURNING id, name, key, start_item_id`, [name, key, startItemId], (error: Error, res: QueryResult) => {
                resolve(this.convert(res)[0])
            })
        }))
    }

    update(name: string, key: string, id: number): Promise<Bundle> {
        return new Promise<Bundle>((resolve => {
            pool.query(`UPDATE bundle
                        SET name = $1,
                            key  = $2
                        WHERE id = $3
                        RETURNING id, name, key, start_item_id`, [name, key, id], (error: Error, res: QueryResult) => {
                resolve(this.convert(res)[0])
            })
        }))
    }

    private convert(res: QueryResult | undefined): Bundle[] {
        if (res !== undefined) {
            return res.rows.map((row) => {
                return new Bundle(
                    Number.parseInt(row["id"]),
                    row["name"],
                    row["key"],
                    Number.parseInt(row["start_item_id"]),
                    []
                )
            })
        } else {
            return []
        }
    }

    setStartItem(id: number, startItemId: number) {
        return new Promise<Boolean>((resolve => {
            pool.query(`UPDATE bundle
                        SET start_item_id = $1
                        WHERE id = $2`, [startItemId, id], (error: Error, res: QueryResult) => {
                resolve(error === null || error === undefined)
            })
        }))
    }
}
