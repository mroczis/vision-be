/**
 * Bundle item representation for create or update operations.
 */
export interface BundleItemSkeleton {
    name: string
    url: string,
    isStart: boolean
}
