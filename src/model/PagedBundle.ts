import {Bundle} from "./Bundle";

/**
 * Server response containing bundles
 */
export class PagedBundle {
    readonly bundles: Array<Bundle>
    readonly count: number

    constructor(bundles: Array<Bundle>, count: number) {
        this.bundles = bundles
        this.count = count
    }
}
