import {BundleDao} from "../dao/BundleDao";

/**
 * Deletes bundle by id. Returns false if bundle did not exist before, otherwise true
 */
export class DeleteBundleUC {

    private readonly bundleDao = new BundleDao()

    execute(request: DeleteBundleUC.Request) : DeleteBundleUC.Response {
        return new DeleteBundleUC.Response(this.bundleDao.delete(request.id))
    }

}

export namespace DeleteBundleUC {

    export class Request {
        private readonly _id: number

        constructor(id: number) {
            this._id = id
        }

        get id(): number {
            return this._id
        }

    }

    export class Response {
        private readonly _success: Promise<boolean>

        constructor(success: Promise<boolean>) {
            this._success = success
        }

        get success(): Promise<boolean> {
            return this._success
        }

    }

}
