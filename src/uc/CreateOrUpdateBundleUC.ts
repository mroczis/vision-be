import {BundleDao} from "../dao/BundleDao";
import {BundleItemDao} from "../dao/BundleItemDao";
import {InsertItemsUC} from "./InsertItemsUC";
import {GetBundleUC} from "./GetBundleUC";
import {Bundle} from "../model/Bundle";
import {BundleItemSkeleton} from "../model/BundleItemSkeleton";

/**
 * Creates or updates bundle and its items.
 * Items update is always fully destructive (old ones are deleted and ones created)
 */
export class CreateOrUpdateBundleUC {

    private readonly bundleDao = new BundleDao()
    private readonly bundleItemDao = new BundleItemDao()

    execute(request: CreateOrUpdateBundleUC.Request): CreateOrUpdateBundleUC.Response {
        let promise: Promise<Bundle>;
        // Create or update bundle
        if (request.id === null) {
            promise = this.bundleDao.insert(request.name, request.key)
        } else {
            promise = this.bundleDao.update(request.name, request.key, request.id)
        }

        const resultBundle = promise.then((bundle) => {
            let deleteResult: Promise<Boolean>;
            if (request.id !== null) {
                // Remove old items if they existed
                deleteResult = this.bundleItemDao.deleteByBundle(bundle.id)
            } else {
                deleteResult = new Promise((res => res(true)))
            }

            return deleteResult.then(_ => {
                // Insert all new items
                const subtasks = new InsertItemsUC().execute(new InsertItemsUC.Request(bundle, request.items))
                return Promise.all(subtasks.promises).then(() => {
                    return new GetBundleUC().execute(new GetBundleUC.Request(bundle.key)).bundle
                })
            })
        })

        return new CreateOrUpdateBundleUC.Response(resultBundle)
    }

}

export namespace CreateOrUpdateBundleUC {

    export class Request {
        private readonly _id: number | null
        private readonly _key: string
        private readonly _name: string
        private readonly _items: Array<BundleItemSkeleton>


        constructor(id: number | null, key: string, name: string, items: Array<BundleItemSkeleton>) {
            this._id = id
            this._key = key
            this._name = name
            this._items = items
        }

        get id(): number | null {
            return this._id
        }

        get key(): string {
            return this._key
        }

        get name(): string {
            return this._name
        }

        get items(): Array<any> {
            return this._items
        }
    }

    export class Response {
        private readonly _bundle: Promise<Bundle | null>

        constructor(bundle: Promise<Bundle | null>) {
            this._bundle = bundle;
        }

        get bundle(): Promise<Bundle | null> {
            return this._bundle
        }
    }
}
