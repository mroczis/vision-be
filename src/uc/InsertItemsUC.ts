import {BundleDao} from "../dao/BundleDao";
import {BundleItemDao} from "../dao/BundleItemDao";
import {Bundle} from "../model/Bundle";
import {BundleItem} from "../model/BundleItem";
import {BundleItemSkeleton} from "../model/BundleItemSkeleton";

/**
 * Inserts new bundle items into database adjusting starting item for bundle.
 * Returns inserted results
 */
export class InsertItemsUC {

    private readonly bundleDao = new BundleDao()
    private readonly bundleItemDao = new BundleItemDao()

    execute(request: InsertItemsUC.Request): InsertItemsUC.Response {
        const promises = request.items.map((it: BundleItemSkeleton) => {
            const {name, url, isStart} = it
            const promise = this.bundleItemDao.insert(request.bundle.id, name, url)

            if (isStart) {
                promise.then((item) => {
                    this.bundleDao.setStartItem(request.bundle.id, item.id)
                })
            }

            return promise;
        })

        return new InsertItemsUC.Response(promises)
    }
}

export namespace InsertItemsUC {

    export class Request {
        private readonly _bundle: Bundle
        private readonly _items: Array<BundleItemSkeleton>

        constructor(bundle: Bundle, items: Array<BundleItemSkeleton>) {
            this._bundle = bundle
            this._items = items
        }

        get items(): Array<any> {
            return this._items
        }

        get bundle(): Bundle {
            return this._bundle
        }
    }

    export class Response {
        private readonly _promises: Array<Promise<BundleItem>>

        constructor(promises: Array<Promise<BundleItem>>) {
            this._promises = promises
        }

        get promises(): Array<Promise<BundleItem>> {
            return this._promises
        }
    }
}
