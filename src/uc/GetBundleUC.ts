import {Bundle} from "../model/Bundle";
import {BundleDao} from "../dao/BundleDao";
import {BundleItemDao} from "../dao/BundleItemDao";

/**
 * Fetches bundle byt its key.
 * Returns given bundle or null.
 */
export class GetBundleUC {

    private readonly bundleDao = new BundleDao()
    private readonly bundleItemDao = new BundleItemDao()

    execute(request: GetBundleUC.Request): GetBundleUC.Response {
        const promise = new Promise<Bundle | null>((res) => {
            this.bundleDao.getByKey(request.key).then((match) => {
                if (match === undefined) {
                    res(null)
                } else {
                    this.bundleItemDao.getByBundle(match.id).then((items) => {
                        const response = match.withItems(items);
                        res(response)
                    })
                }
            })
        });

        return new GetBundleUC.Response(promise)
    }

}

export namespace GetBundleUC {

    export class Request {
        private readonly _key: string

        constructor(key: string) {
            this._key = key
        }

        get key(): string {
            return this._key
        }
    }

    export class Response {
        private readonly _bundle: Promise<Bundle | null>

        constructor(bundle: Promise<Bundle | null>) {
            this._bundle = bundle
        }

        get bundle(): Promise<Bundle | null> {
            return this._bundle
        }
    }

}
