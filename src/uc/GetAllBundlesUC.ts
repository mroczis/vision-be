import {Bundle} from "../model/Bundle";
import {BundleDao} from "../dao/BundleDao";
import {BundleItemDao} from "../dao/BundleItemDao";
import {PagedBundle} from "../model/PagedBundle";

/**
 * Fetches all bundles that match specified requirements, mainly limit, offset and query.
 * Query must be part of key or name of given bundle.
 */
export class GetAllBundlesUC {

    private readonly bundleDao = new BundleDao()
    private readonly bundleItemDao = new BundleItemDao()

    execute(request: GetAllBundlesUC.Request): GetAllBundlesUC.Response {
        const items = this.bundleDao.getAll(request.limit, request.offset, request.query).then((bundles) => {
            const ps = bundles.map((bundle => {
                return this.bundleItemDao.getByBundle(bundle.id)
            }));

            return Promise.all(ps).then((items) => {
                const response: Array<Bundle> = []
                for (let i = 0; i < items.length; i++) {
                    response.push(bundles[i].withItems(items[i]))
                }

                return response
            })
        })
        const total = this.bundleDao.getAllCount(request.query)

        const result = Promise.all([items, total]).then(([data, count]) => {
            return new PagedBundle(data, count)
        })

        return new GetAllBundlesUC.Response(result)
    }

}

export namespace GetAllBundlesUC {

    export class Request {
        private readonly _limit: number
        private readonly _offset: number
        private readonly _query: string | undefined

        constructor(limit: number, offset: number, query: string | undefined) {
            this._limit = limit
            this._offset = offset
            this._query = query
        }

        get limit(): number {
            return this._limit
        }

        get offset(): number {
            return this._offset
        }

        get query(): string | undefined {
            return this._query
        }
    }

    export class Response {
        private readonly _bundles: Promise<PagedBundle>

        constructor(bundle: Promise<PagedBundle>) {
            this._bundles = bundle
        }

        get bundles(): Promise<PagedBundle> {
            return this._bundles
        }
    }

}
