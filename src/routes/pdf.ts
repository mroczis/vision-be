import {RequestHandler} from "express";

const PDFImage = require("pdf-image").PDFImage;
const fs = require("fs");
const tmp = require("tmp");

/**
 * Converts PDF document to image
 *  - form param 'pdf' -> PDF document
 *
 * Returns JPEG image in body if successful.
 */
export const postPdf: RequestHandler = (req, res) => {
    const file = req.file

    if (file && file.mimetype === "application/pdf") {
        const tmpobj = tmp.fileSync();
        fs.writeFileSync(tmpobj.name, file.buffer);

        const pdfImage = new PDFImage(tmpobj.name, {
            combinedImage: true
        });

        pdfImage.convertFile().then((path: string) => {
            tmpobj.removeCallback()
            res.download(path)
        }).catch((e: any) => {
            tmpobj.removeCallback()
            console.log(e)
            res.status(500).send()

        })
    } else {
        res.status(400).send()
    }
}
