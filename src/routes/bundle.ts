import {RequestHandler} from "express";
import {GetBundleUC} from "../uc/GetBundleUC";
import {CreateOrUpdateBundleUC} from "../uc/CreateOrUpdateBundleUC";
import {DeleteBundleUC} from "../uc/DeleteBundleUC";
import {GetAllBundlesUC} from "../uc/GetAllBundlesUC";
import {getNumber} from "../util/calculation";

export const getAllBundles: RequestHandler = (req, res) => {
    const limit = getNumber(req.query.limit, 20)
    const offset = getNumber(req.query.offset, 0)
    const rawQuery = req.query.query
    let query = undefined
    if (typeof rawQuery === "string") {
        query = rawQuery
    }

    new GetAllBundlesUC().execute(new GetAllBundlesUC.Request(limit, offset, query)).bundles.then((bundles) => {
        res.contentType("application/json").send(JSON.stringify(bundles))
    })
}

export const getBundle: RequestHandler = (req, res) => {
    const key = req.params["key"]
    new GetBundleUC().execute(new GetBundleUC.Request(key)).bundle.then((bundle) => {
        if (bundle === null) {
            res.status(404).send('')
        } else {
            res.contentType("application/json").send(JSON.stringify(bundle))
        }
    })
}

export const createBundle: RequestHandler = (req, res) => {
    if (validateBundleBody(req.body)) {
        const {name, key, items} = req.body

        new CreateOrUpdateBundleUC().execute(new CreateOrUpdateBundleUC.Request(null, key, name, items)).bundle.then((bundle) => {
            if (bundle === null) {
                res.status(404).send('')
            } else {
                res.status(200).contentType("application/json").send(JSON.stringify(bundle))
            }
        })
    } else {
        res.status(400).send()
    }
}

export const updateBundle: RequestHandler = (req, res) => {
    const id = getNumber(req.params["bundleId"], 0)
    if (id > 0 && validateBundleBody(req.body)) {
        const {name, key, items} = req.body

        new CreateOrUpdateBundleUC().execute(new CreateOrUpdateBundleUC.Request(id, key, name, items)).bundle.then((bundle) => {
            if (bundle === null) {
                res.status(404).send('')
            } else {
                res.status(200).contentType("application/json").send(JSON.stringify(bundle))
            }
        })

    } else {
        res.status(400).send()
    }
}

export const deleteBundle: RequestHandler = (req, res) => {
    const id = getNumber(req.params["bundleId"], 0)
    if (id > 0) {
        new DeleteBundleUC().execute(new DeleteBundleUC.Request(id)).success.then((ok) => {
            if (ok) {
                res.status(204).send('')
            } else {
                res.status(404).send('')
            }
        })
    } else {
        res.status(404).send('')
    }
}

/**
 * Since JS does not care about types and properties we must check for them manually
 */
function validateBundleBody(body: any) {
    if (body.name && body.key && Array.isArray(body.items)) {
        const items: Array<any> = body.items
        const invalid = items.find((it) =>
            !it.name || !it.url || !(it.isStart === true || it.isStart === false)
        ) !== undefined

        return !invalid
    } else {
        return false
    }

}
