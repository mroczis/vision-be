import puppeteer from "puppeteer";
import {RequestHandler} from "express";
import {getNumber} from "../util/calculation";

/**
 * Fetches screenshot of given website
 *  - query param 'url' -> will take screenshot of given website
 *  - query param 'width' -> result will have given width
 *
 * Returns JPEG image in body if successful.
 */
export const getScreenshot: RequestHandler = (req, res) => {
    const rawUrl = req.query.url;
    const rawWidth = req.query.width;

    if (typeof rawUrl === 'string') {
        const url = <string>rawUrl;
        const width = getNumber(<string> rawWidth, 800);
        const height = Math.floor(width * 0.75);

        if (isHttpUrl(url)) {
            const downloadScreenshot = async () => {
                const browser = await puppeteer.launch({args: ['--no-sandbox']});
                const page = await browser.newPage();
                await page.setViewport({width: width, height: height});
                await page.goto(<string>url);
                const buffer: Buffer = await page.screenshot({fullPage: true, type: "jpeg", encoding: "binary"});
                await browser.close();

                return buffer;
            };

            downloadScreenshot()
                .then((buffer) => {
                    res.contentType('image/jpeg');
                    res.end(buffer, 'binary');
                })
                .catch(err => {
                    console.log(err)
                    res.status(500).send({error: "Cannot fetch image", trace: err})
                })
        } else {
            res.status(400).send({error: "Provided URL is not valid HTTP(s) URL", url: url})
        }
    } else {
        res.status(400).send({error: "Missing query parameter 'url'"})
    }
};

/**
 * Validates if parameter of this function is HTTP(S) url
 * @param string URL candidate
 */
function isHttpUrl(string: string): boolean {
    try {
        const url = new URL(string);
        const protocol = url.protocol.toLowerCase();
        return protocol.startsWith("http") || protocol.startsWith("https")
    } catch (_) {
        return false;
    }
}
