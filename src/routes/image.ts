import request from "request";
import {RequestHandler} from "express";

/**
 * Fetches image from destination server and streams it back to requestor
 * @param req
 * @param res
 */
export const getImage : RequestHandler = (req, res) => {
    const rawUrl = req.query.url;
    if (typeof rawUrl === 'string') {
        const url = <string> rawUrl;
        request.get(url).pipe(res);
    } else {
        res.status(400).send({error: "Missing query parameter 'url'"})
    }
}
