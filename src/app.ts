import express from "express"
import basicAuth from "express-basic-auth"
import cors from 'cors'
import {getScreenshot} from "./routes/screenshot"
import {getImage} from "./routes/image"
import {createBundle, deleteBundle, getAllBundles, getBundle, updateBundle} from "./routes/bundle"
import {postPdf} from "./routes/pdf"
import multer from "multer"
import {login} from "./routes/login"
import {PoolClient} from "pg";

require('dotenv').config()

const app = express()
const publicRouter = express.Router()
const privateRouter = express.Router()

let users;
if (process.env.DEBUG) {
    users = {
        admin: <string>process.env.AUTH_PASSWORD,
        test: 'test'
    }
} else {
    users = {
        admin: <string>process.env.AUTH_PASSWORD,
    }
}

const auth = basicAuth({users: users})

const upload = multer({});
const Pool = require('pg').Pool
export const pool = new Pool({
    user: <string>process.env.DB_USER,
    host: <string>process.env.DB_HOST,
    database: <string>process.env.DB_DATABASE,
    password: <string>process.env.DB_PASSWORD,
    port: parseInt(<string>process.env.DB_PORT),
})

// Register routes
publicRouter.get('/screenshot', getScreenshot)
publicRouter.get('/image', getImage)
publicRouter.get('/bundle/:key', getBundle)
publicRouter.post('/pdf', upload.single('pdf'), postPdf)

privateRouter.get('/login', login)
privateRouter.get('/bundle', getAllBundles)
privateRouter.post('/bundle', createBundle)
privateRouter.put('/bundle/:bundleId', updateBundle)
privateRouter.delete('/bundle/:bundleId', deleteBundle)


app.use(express.json())
app.use(cors())
app.use('/', publicRouter)
app.use('/', auth, privateRouter)

app.listen(process.env.PORT, () => {
    console.log(`⚡️[server]: Server is running / ${process.env.PORT}`)
})

pool.on('error', (err: Error, client: PoolClient) => {
    console.error('Unexpected error on idle client', err)
    process.exit(-1)
})

// Logging of queries
const Query = require('pg').Query;
const submit = Query.prototype.submit;
Query.prototype.submit = function () {
    const text = this.text;
    const values = this.values;
    const query = values.reduce((q: any, v: any, i: any) => q.replace(`$${i + 1}`, v), text);
    console.log(query);
    submit.apply(this, arguments);
};

export default app;
